package com.abs.assignment.scala

class test{
  def conditions(x:Int):Unit={
    if(x>10){
      printStatement(println("x is 10"))
    }else if(x>20){
      printStatement(println("x is 20"))
    }else if(x>=30){
      printStatement(println("x is 30"))
    }else{
      printStatement(println("x is 40"))
    }
  }
  def printStatement[p](f: =>p):Unit={
   f
  }
  
}

object ifElseClass {
  def main(args: Array[String]) = {
    var t=new test()
    
    t.conditions(10)
    t.conditions(20)
    t.conditions(30)
    t.conditions(40)
  }
}