package com.abs.assignment.scala


class LoopControlClass(){
  def forLoop(){
    for(x <- 1 until 10){
      println(x)
    }
  }
}
object LoopControl {
  def main(args: Array[String]): Unit = {
    val cc=new LoopControlClass()
    cc.forLoop()
  }
}