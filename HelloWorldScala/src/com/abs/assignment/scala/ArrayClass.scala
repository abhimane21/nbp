package com.abs.assignment.scala
import Array._
class ArrayClass {
  def printSingleArray(ss:Array[String]):Unit={
    for(x<-ss){
      println(x);
    }
  }
  def setMultiDimesionArray(ss:Array[Array[String]]){
    for(i<- 0 to 2){
      for(j<-0 to 2){
          ss(i)(j)="Abhi".concat(i.toString())
      }
    }
    
    for(i<-ss){
      for(j<-i){
        println(j);
      }
    }
  }
}

object ArrayClassObject{
  def main(args: Array[String]): Unit = {
    var testVar=new ArrayClass();
    var ss=Array[String]("abhi","amol","did");
    testVar.printSingleArray(ss)
    var twoDimArray=Array.ofDim[String](3,3)
    testVar.setMultiDimesionArray(twoDimArray);
  }
}