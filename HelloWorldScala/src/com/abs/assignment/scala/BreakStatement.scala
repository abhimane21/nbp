package com.abs.assignment.scala

import scala.util.control.Breaks


class LoopBreakClass{
 def breakStatement():Unit={
   var bk=new Breaks
   bk.breakable(
    for(x <- 1 to 10){
      println(x);
      if(x==5) 
        bk.break()
    }
   )
    println("Ended")
 }
}

class mulitpleLoopBreakStatement{
  def breakStatement1():Unit={
    var inner=new Breaks
    var outer=new Breaks
    inner.breakable(
      for(x<- 1 to 5){
        println(x)
         if(x==3)
           inner.break()
         outer.breakable(
           for(y<- 1 to 5){
             println(y)
             if(y==4)
                outer.break()
           }
         )
      }
    )
    println("finally")
  }
}

object BreakStatement {
  def main(args: Array[String]): Unit = {
    var lpc=new LoopBreakClass();
    lpc.breakStatement()
    var ml=new mulitpleLoopBreakStatement()
    ml.breakStatement1()
  }
}