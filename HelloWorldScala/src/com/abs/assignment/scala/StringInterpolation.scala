package com.abs.assignment.scala

object StringInterpolation {
  def main(args: Array[String]): Unit = {
    var name :String="Abhijeet"
    var strappend=s"Hello\t \n $name"
    var strappendRaw=raw"Hello\t \n $name"
    println(strappend)
    println(strappendRaw)
  }
}