package com.abs.assignment.scala

object ScalaCaseDemo {
  def main(args: Array[String]): Unit = {
    var x:Int=11;
    x match{
      case 10 => println("Hello")
       case _ => println("Bye")
    }
    
    var y:Any="Abhi"
    y match {
      case 10=> println("1")
      case "Abhi"=> println("Abhi");
    }
    
    var t1=new mm(1,2)
   
     t1 match {
      case mm(10,20)=> println("1")
      case mm(20,30)=> println("2")
      case _=> println("3")
    }
    
    
  }
}

case class mm(x:Int,y:Int)