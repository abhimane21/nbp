package com.abs.assignment.scala

class functionbyVariableArg{
  def variableArgs(args:String*):Unit={
    for(x <- args){
      print(x+"\t")
    }
  }
}

object functionbyVariableArg {
  def main(args: Array[String]): Unit = {
    var c=new functionbyVariableArg();
    c.variableArgs("a","b","c")
  }
}