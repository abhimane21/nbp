package com.abs.assignment.scala

object TraitsDemo {
  def main(args: Array[String]): Unit = {
    var p=new traitsImpl(10,20)
    var p1=new traitsImpl(10,20)
    var p2=new traitsImpl(10,20)
    println(p.isEqual(p1))
  }
}

trait testDemo{
  def isEqual(x:Any):Boolean
  def isNotEqual(x:Any):Boolean = (!isEqual(x))
}

class traitsImpl(x:Int,y:Int) extends testDemo{
  var xx:Int=x
  var yy:Int=y
  def isEqual(obj:Any):Boolean={
    obj.isInstanceOf[traitsImpl] && obj.asInstanceOf[traitsImpl].xx==xx
  }
}