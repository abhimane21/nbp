package com.abs.assignment.scala


class PointsSingleton(x:Int,y:Int){
  var xx:Int=x
  var yy:Int=y
  
  
}

/**
 * Object is singleton
 */
object SingleTonObject {
  def main(args: Array[String]): Unit = {
    def printObj{
      var cc=new PointsSingleton(10,20)
      println(cc.xx)
      println(cc.yy)
    }
    printObj
  }
}