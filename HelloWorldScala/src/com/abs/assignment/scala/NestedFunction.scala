package com.abs.assignment.scala

object NestedFunction {
     
   def main(args: Array[String]) {
      println( factorial(0) )
      println( factorial(1) )
      println( factorial(2) )
      println( factorial(3) )
      
     println(factorial _)
   }
   
   def factorial(i: Int): Int = {
      def fact(i: Int, accumulator: Int): Int = {
        println(accumulator)
        if (i <= 1)
           accumulator
        else
           fact(i - 1, i * accumulator)
      }
      fact(i, 1)
   }
}