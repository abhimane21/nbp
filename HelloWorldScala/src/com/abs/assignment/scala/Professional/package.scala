package com.abs.assignment.scala

package Professional {
  package employee{
    class executive{
      private[Professional] var x:Int=1
      protected[executive] var y:Int=2
      protected[this] var z=3
      def accessModifier(another:executive){
        println(another.x);
        println(another.y);
      //  println(another.z); Error (Will be accesible only through the implicit variable
      }
    }
  }
}