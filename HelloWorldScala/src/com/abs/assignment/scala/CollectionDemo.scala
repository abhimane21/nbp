package com.abs.assignment.scala

object CollectionDemo {
  def main(args: Array[String]): Unit = {
    var tDemo=new test()
    //tDemo.listDemo()
    //tDemo.reverseList
    tDemo.concatText
  }
  class test{
    def listDemo():Unit={
      var s=List(10,20)
      for(p<-s){
        println(p);
      }
      
      var p=List.fill(1)("A")
      for(t<-p){
        println(t);
      }
      
      var r=List.tabulate(2)(n => n*n)
      var t=List.tabulate(3, 3)((n1,n2) => (n1+n2))
      for(abhi <-  t){
        println(abhi)
      }
      
    }
    
    def reverseList:Unit={
      var rvrList=List("A","B","C","D")
      for(zz <- rvrList.reverse) println(zz)
    }
    
    def concatText:Unit={
      var rvrList1=List("A","B","C","D")
      var rvrList2=List("A","B","C","D")
      for(d <- List.concat(rvrList1,rvrList2)) println(d)
      for(e <-("Abhi"::("Mane":: ("Namdeo") :: Nil))) println(e)
    }
    
  }
  
}

